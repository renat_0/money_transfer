package com.mt.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@Getter
public class SendMoneyRequest {
    @NotNull
    Integer userIdFrom;
    @NotNull
    Integer userIdTo;
    @Pattern(regexp= "USD|RUB|GBP")
    String currency;
    @Pattern(regexp = "[0-9]+.?[0-9]+")
    String moneyStringRep;
}
