package com.mt.exception.checked;

public class UserAccountDoestExistException  extends Exception {

    public UserAccountDoestExistException() {
        super("User account doesn't exist");
    }

}
