package com.mt.exception.checked;

public class ToAccountDoesntExistException extends Exception {
    public ToAccountDoesntExistException() {
        super("To-account doesn't exist");
    }

}
