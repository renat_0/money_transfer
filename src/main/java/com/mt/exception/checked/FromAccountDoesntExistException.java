package com.mt.exception.checked;

public class FromAccountDoesntExistException extends Exception {

    public FromAccountDoesntExistException() {
        super("From-account doesn't exist");
    }

}
