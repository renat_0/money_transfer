package com.mt.exception.checked;

public class IdAlreadyExistsException extends Exception {

    public IdAlreadyExistsException() {
        super("Such id already exists");
    }

}