package com.mt.exception.checked;

public class InsufficientCashAccountException extends Exception {
    public InsufficientCashAccountException() {
        super("Insufficient money in the from-account");
    }

}
