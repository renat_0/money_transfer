package com.mt.service;

import com.mt.dao.AccountDao;
import com.mt.dao.UserAccountDao;
import com.mt.dto.AccountInfoDto;
import com.mt.dto.StatusDto;
import com.mt.request.SendMoneyRequest;
import com.mt.request.UpTheBalanceRequest;

import javax.inject.Singleton;
import java.util.Map;

@Singleton
public class AccountsManipulationService {

    private final UserAccountDao userAccountDao;
    private final AccountDao accountDao = AccountDao.getAccountDao();

    public AccountsManipulationService(UserAccountDao userAccountDao) {
        this.userAccountDao = userAccountDao;
    }

    public StatusDto processSendRequest(SendMoneyRequest request) {
        if (request.getUserIdFrom().equals(request.getUserIdTo())) {
            return new StatusDto("Ids are equal to each other");
        }

        String status = "sended";
        try {
            accountDao.sendMoney(
                    request.getUserIdFrom(),
                    request.getUserIdTo(),
                    request.getCurrency(),
                    Double.valueOf(request.getMoneyStringRep()));
        } catch (Exception e) {
            status = e.getMessage();
        }
        return new StatusDto(status);
    }

    public StatusDto createUserAccount(Integer id) {
        String status = "created";
        try {
            accountDao.createAccount(id);
        } catch (Exception e) {
            status = e.getMessage();
        }
        return new StatusDto(status);
    }

    public AccountInfoDto getAccountInfo(Integer id) {
        Map<String, Double> moneyInfo = accountDao.getAccountInfoOrNull(id);
        return AccountInfoDto
                .builder()
                .id(id)
                .moneyInfo(moneyInfo)
                .error(moneyInfo == null ? "user doesn't exists" : "")
                .build();
    }

    public StatusDto upTheBalance(UpTheBalanceRequest request) {
        String status = "money has been increased";
        try {
            accountDao.upTheBalance(
                    request.getUserId(),
                    request.getCurrency(),
                    Double.valueOf(request.getMoneyStringRep()));
        } catch (Exception e) {
            status = e.getMessage();
        }
        return new StatusDto(status);
    }
}
