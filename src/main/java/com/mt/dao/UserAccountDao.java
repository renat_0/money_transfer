package com.mt.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.customizers.SingleValueResult;

public interface UserAccountDao {

    @GetGeneratedKeys
    @SqlUpdate("insert into MY_APPLICATION.USERS(first_name)"
            + " values (:first_name)")
    Long createUserAccount(@Bind("first_name") String  firstName);


    @SqlQuery("select first_name from mt_application.users where id = ?")
    String getName(long id);
}
