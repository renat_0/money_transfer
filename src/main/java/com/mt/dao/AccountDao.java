package com.mt.dao;

import com.mt.exception.checked.*;
import lombok.Synchronized;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AccountDao {

    private static AccountDao accountDao;

    private AccountDao() {
    }

    private Map<Integer, Map<String, Double>> inMemoryDb = new ConcurrentHashMap<Integer, Map<String, Double>>();

    @Synchronized
    public void sendMoney(Integer from, Integer to, String currency, Double amount)
            throws FromAccountDoesntExistException, ToAccountDoesntExistException, InsufficientCashAccountException
    {
        Map<String, Double> acc1 = inMemoryDb.get(from);
        if (acc1 == null) {
            throw new FromAccountDoesntExistException();
        }
        Map<String, Double> acc2 = inMemoryDb.get(to);
        if (acc2 == null) {
            throw new ToAccountDoesntExistException();
        }
        Double cash = acc1.getOrDefault(currency, 0.0);
        if (cash < amount) {
            throw new InsufficientCashAccountException();
        } else {
            cash -= amount;
            acc1.put(currency, cash - amount);
            acc2.put(currency, acc2.getOrDefault(currency, 0.0) + amount);
        }
    }

    @Synchronized
    public void createAccount(Integer id) throws IdAlreadyExistsException {
        if (inMemoryDb.containsKey(id)) {
            throw new IdAlreadyExistsException();
        }
        inMemoryDb.put(id, new ConcurrentHashMap<>());
    }

    @Synchronized
    public Map<String,Double> getAccountInfoOrNull(Integer id) {
        Map<String,Double> res = inMemoryDb.get(id);
        return res;
    }

    @Synchronized
    public void upTheBalance(Integer id, String currency, Double amount) throws UserAccountDoestExistException {
        Map<String, Double> acc1 = inMemoryDb.get(id);
        if (acc1 == null) {
            throw new UserAccountDoestExistException();
        }
        acc1.put(currency, acc1.getOrDefault(currency, 0.0) + amount);
    }

    public static AccountDao getAccountDao() {
        if (accountDao == null) {
            accountDao = new AccountDao();
        }
        return accountDao;
    }

}
