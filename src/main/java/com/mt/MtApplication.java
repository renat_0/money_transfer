package com.mt;

import com.mt.config.AppConfig;
import com.mt.dao.UserAccountDao;
import com.mt.resource.RESTMoneySenderController;
import com.mt.service.AccountsManipulationService;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.skife.jdbi.v2.DBI;

public class MtApplication extends Application<AppConfig> {
    public static void main(String[] args) throws Exception {

        new MtApplication().run(
                "server",
                "config.yml"
        );
    }

    @Override
    public String getName() {
        return "MyApplication";
    }

    @Override
    public void initialize(Bootstrap<AppConfig> bootstrap) {
        bootstrap.addBundle(new FlywayBundle<AppConfig>() {
            @Override
            public DataSourceFactory getDataSourceFactory(AppConfig configuration) {
                return configuration.getDataSourceFactory();
            }

            @Override
            public FlywayFactory getFlywayFactory(AppConfig configuration) {
                return configuration.getFlywayFactory();
            }
        });

        bootstrap.addBundle(new SwaggerBundle<AppConfig>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(AppConfig configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    @Override
    public void run(
            AppConfig configuration,
            Environment environment) {

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "h2");

        final UserAccountDao userDAO = jdbi.onDemand(UserAccountDao.class);

        final RESTMoneySenderController resource = new RESTMoneySenderController(
                new AccountsManipulationService(userDAO));
        environment.jersey().register(resource);
    }

}