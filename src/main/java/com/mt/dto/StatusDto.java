package com.mt.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@RequiredArgsConstructor
public class StatusDto {

    public StatusDto(String status) {
        this.status = status;
    }

    String status;
}
