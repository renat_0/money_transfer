package com.mt.resource;

import com.mt.dto.AccountInfoDto;
import com.mt.dto.StatusDto;
import com.mt.request.SendMoneyRequest;
import com.mt.request.UpTheBalanceRequest;
import com.mt.service.AccountsManipulationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


@Path("/money")
@Api("/money")
public class RESTMoneySenderController {


    private final AccountsManipulationService accountsManipulationService;

    @Inject
    public RESTMoneySenderController(AccountsManipulationService accountsManipulationService) {
        this.accountsManipulationService = accountsManipulationService;
    }

    @POST
    @Path("/send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation("Method for sending money from one an account to an another")
    public StatusDto sendMoney(@Valid SendMoneyRequest request) {
        return accountsManipulationService.processSendRequest(request);
    }

    @GET
    @Path("/create/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Create account with id")
    public StatusDto createAccount(@QueryParam("userId") Integer newId) {
        return accountsManipulationService.createUserAccount(newId);
    }

    @GET
    @Path("/info/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Get info of account")
    public AccountInfoDto getAccountInfo(@QueryParam("userId") Integer newId) {
        return accountsManipulationService.getAccountInfo(newId);
    }


    @POST
    @Path("deposite/up/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Up the balance of account")
    public StatusDto upTheBalance(@Valid UpTheBalanceRequest request) {
        return accountsManipulationService.upTheBalance(request);
    }



}
